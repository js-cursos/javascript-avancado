import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetalheAlbumComponent } from './detalhe-album/detalhe-album.component';
import { FormAlbumComponent } from './form-album/form-album.component';
import { ListaAlbumComponent } from './lista-album/lista-album.component';

@NgModule({
    declarations: [
        ListaAlbumComponent,
        FormAlbumComponent,
        DetalheAlbumComponent
    ],
    imports: [
        HttpClientModule,
        CommonModule
    ]
})
export class AlbumModule { }