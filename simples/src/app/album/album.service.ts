import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Album } from './album';

@Injectable({
    providedIn: "root"
})
export class AlbumService {

    constructor(private http: HttpClient) { }

    listar() {
        return this.http.get<Album[]>('http://localhost');
    }
}