import { Component, OnInit } from '@angular/core';

import { Album } from '../album';
import { AlbumService } from '../album.service';

@Component({
  selector: 'simples-lista-album',
  templateUrl: './lista-album.component.html',
  styleUrls: ['./lista-album.component.css']
})
export class ListaAlbumComponent implements OnInit {

  albuns: Album[] = [];

  //activatedRoute parametros na rota
  constructor(private albumService: AlbumService) { }

  ngOnInit(): void {
    this.albumService.listar().subscribe(albuns => this.albuns = albuns);
  }

}
