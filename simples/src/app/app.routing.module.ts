import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaAlbumComponent } from './album/lista-album/lista-album.component';
import { FormAlbumComponent } from './album/form-album/form-album.component';

const routes: Routes = [
    { path: '', redirectTo: 'lista', pathMatch: 'full' },
    { path: 'lista', component: ListaAlbumComponent },
    { path: 'novo', component: FormAlbumComponent },
    { path: '**', component: ListaAlbumComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}